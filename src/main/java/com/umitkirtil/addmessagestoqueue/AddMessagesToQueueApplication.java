package com.umitkirtil.addmessagestoqueue;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AddMessagesToQueueApplication {

    public static void main(String[] args) {
        SpringApplication.run(AddMessagesToQueueApplication.class, args);
    }

}