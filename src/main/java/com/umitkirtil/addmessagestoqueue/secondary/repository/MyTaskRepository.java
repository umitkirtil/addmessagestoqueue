package com.umitkirtil.addmessagestoqueue.secondary.repository;

import com.umitkirtil.addmessagestoqueue.secondary.domain.MyTask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MyTaskRepository extends JpaRepository<MyTask, Long> {
}