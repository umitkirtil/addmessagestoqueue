package com.umitkirtil.addmessagestoqueue.secondary.domain;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Getter
@Setter
@Entity(name = "tasks")
public class MyTask {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "userid")
    private Long userid;

    @Column(name = "taskname")
    private String taskname;
}
