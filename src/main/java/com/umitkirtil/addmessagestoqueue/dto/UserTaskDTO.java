package com.umitkirtil.addmessagestoqueue.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class UserTaskDTO {

    //from users table ...
    private String userid;

    //from Tasks table ...
    private List<String> tasks = new ArrayList<>();

    @Override
    public String toString() {
        return "UserTaskDTO{" +
                "userid='" + userid + '\'' +
                ", tasks sayısı=" + tasks.size() +
                '}';
    }
}
