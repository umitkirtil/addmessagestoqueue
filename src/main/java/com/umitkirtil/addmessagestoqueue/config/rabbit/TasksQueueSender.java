package com.umitkirtil.addmessagestoqueue.config.rabbit;

import com.umitkirtil.addmessagestoqueue.dto.UserTaskDTO;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TasksQueueSender {

    public final String topicExchangeName = "tasks-exchange";
    public final String queueName = "tasks";
    public final String routingKey = "task.taskdetail.task";

    @Autowired
    RabbitTemplate rabbitTemplate;

    public void send(UserTaskDTO userTaskDTO){
        rabbitTemplate.convertAndSend(topicExchangeName , routingKey , userTaskDTO);
        System.out.println("Message Sent to Queue 'tasks' " + userTaskDTO);
    }

}
