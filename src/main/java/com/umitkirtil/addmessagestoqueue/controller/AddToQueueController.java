package com.umitkirtil.addmessagestoqueue.controller;

import com.umitkirtil.addmessagestoqueue.config.rabbit.TasksQueueSender;
import com.umitkirtil.addmessagestoqueue.dto.UserTaskDTO;
import com.umitkirtil.addmessagestoqueue.primary.domain.Task;
import com.umitkirtil.addmessagestoqueue.primary.domain.User;
import com.umitkirtil.addmessagestoqueue.primary.service.TaskService;
import com.umitkirtil.addmessagestoqueue.primary.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AddToQueueController {

    @Autowired
    UserService userService;

    @Autowired
    TaskService taskService;

    @Autowired
    TasksQueueSender tasksQueueSender;

    User currentUser;

    @GetMapping(value = "/addTaskToQueue")
    public String producer(@RequestParam("userID") String userID) {

        currentUser = userService.getUserByID(Long.valueOf(userID));

        if (currentUser != null) {
            List<Task> taskList = taskService.findAllTasksByUser(currentUser);

            UserTaskDTO userTaskDTO = new UserTaskDTO();
            userTaskDTO.setUserid(String.valueOf(currentUser.getId()));
            taskList.forEach(task -> {
                userTaskDTO.getTasks().add(task.getTaskname());
            });

            tasksQueueSender.send(userTaskDTO);
            return "Message added to Tasks Queue Successfully";
        } else {
            return "Message could not send.";
        }
    }

}
