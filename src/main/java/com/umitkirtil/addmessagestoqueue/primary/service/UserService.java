package com.umitkirtil.addmessagestoqueue.primary.service;

import com.umitkirtil.addmessagestoqueue.primary.domain.User;
import com.umitkirtil.addmessagestoqueue.primary.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public User addUser(User user) {
        return userRepository.save(user);
    }

    public User getUserByID(Long id) {
        return userRepository.findById(id).orElse(null);
    }

}
