package com.umitkirtil.addmessagestoqueue.primary.service;

import com.umitkirtil.addmessagestoqueue.primary.domain.Task;
import com.umitkirtil.addmessagestoqueue.primary.domain.User;
import com.umitkirtil.addmessagestoqueue.primary.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskService {

    @Autowired
    TaskRepository taskRepository;

    public Task findTaskByTaskID(Long id) {
        return taskRepository.findById(id).get();
    }

    public List<Task> findAllTasksByUser(User user) {
        return taskRepository.findAllByUser(user);
    }

    public Task addTask(Task task) {
        return taskRepository.save(task);
    }

}
