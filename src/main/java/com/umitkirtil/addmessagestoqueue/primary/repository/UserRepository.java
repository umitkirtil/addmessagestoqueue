package com.umitkirtil.addmessagestoqueue.primary.repository;

import com.umitkirtil.addmessagestoqueue.primary.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
}
