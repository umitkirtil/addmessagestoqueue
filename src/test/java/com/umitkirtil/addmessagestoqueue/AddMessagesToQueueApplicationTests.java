package com.umitkirtil.addmessagestoqueue;

import com.umitkirtil.addmessagestoqueue.config.rabbit.TasksQueueSender;
import com.umitkirtil.addmessagestoqueue.dto.UserTaskDTO;
import com.umitkirtil.addmessagestoqueue.primary.domain.Task;
import com.umitkirtil.addmessagestoqueue.primary.domain.User;
import com.umitkirtil.addmessagestoqueue.primary.repository.TaskRepository;
import com.umitkirtil.addmessagestoqueue.primary.repository.UserRepository;
import com.umitkirtil.addmessagestoqueue.secondary.domain.MyTask;
import com.umitkirtil.addmessagestoqueue.secondary.repository.MyTaskRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class AddMessagesToQueueApplicationTests {

    @Autowired
    TasksQueueSender tasksQueueSender;

    @Autowired
    MyTaskRepository myTaskRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    TaskRepository taskRepository;

    @Test
    void addDummyUsers() {

        User user = new User();

        Task task = new Task();
        task.setTaskname("Task 4");
        task.setUser(user);

        Task task2 = new Task();
        task2.setTaskname("Task 3");
        task2.setUser(user);

        Task task3 = new Task();
        task3.setTaskname("Task 5");
        task3.setUser(user);

        user.getTasks().add(task);
        user.getTasks().add(task2);
        user.getTasks().add(task3);

        user.setUsername("user 2");

        userRepository.save(user);

    }

    @Test
    void addDummyTaskH2() {

        MyTask myTask = new MyTask();
        myTask.setTaskname("Task 1");
        myTask.setUserid(1l);

        myTaskRepository.save(myTask);

    }

    @Test
    void sendToQueue() {
        UserTaskDTO userTaskDTO = new UserTaskDTO();
        userTaskDTO.setUserid("user3");
        userTaskDTO.getTasks().add("task 6");
        userTaskDTO.getTasks().add("task 7");
        userTaskDTO.getTasks().add("task 9");

        tasksQueueSender.send(userTaskDTO);
    }

}
